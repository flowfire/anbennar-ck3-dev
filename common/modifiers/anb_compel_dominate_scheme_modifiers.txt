﻿### Compel & Dominate ###

compel_personal_insights = {
	icon = social_positive
	scheme_success_chance = 10
	scheme_power = 5
}

compel_personal_insights_direct = {
	icon = social_positive
	scheme_success_chance = 10
	scheme_power = 5
	scheme_secrecy = -10
}

compel_personal_insights_strong = {
	icon = social_positive
	scheme_success_chance = 20
	scheme_power = 10
}

dominate_opposing_presence = {
	icon = magic_negativ
	scheme_success_chance = -20
	scheme_power = -10
	scheme_secrecy = 10
}