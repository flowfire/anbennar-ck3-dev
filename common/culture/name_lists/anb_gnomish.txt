﻿name_list_gnomish = {

	dynasty_names = {
		"dynn_Abdena"
		"dynn_Allena"
		"dynn_Andringa"
		"dynn_Bauwenga"
		"dynn_Beninga"
		"dynn_Boltinge"
		"dynn_Botta"
		"dynn_Buning"
		"dynn_Butzel"
		"dynn_Cammingha"
		"dynn_Cirksema"
		"dynn_Clant"
		"dynn_Coenders"
		"dynn_Cordinghe"
		"dynn_Dives"
		"dynn_Ewsum"
		"dynn_Folkerdinga"
		"dynn_Gaykema"
		"dynn_Ghetzerka"
		"dynn_Gockinga"
		"dynn_Goltraven"
		"dynn_Haijkama"
		"dynn_Harting"
		"dynn_Heddama"
		"dynn_Hedegu"
		"dynn_Hedinge"
		"dynn_Hornada"
		"dynn_Hornekingh"
		"dynn_Houwerda"
		"dynn_Huginge"
		"dynn_Huninga"
		"dynn_Huwenga"
		"dynn_Idzarda"
		"dynn_Ildsisma"
		"dynn_Jarges"
		"dynn_Kalemere"
		"dynn_Kater"
		"dynn_Korenporta"
		"dynn_Kote"
		"dynn_Lewe"
		"dynn_Manninga"
		"dynn_Martena"
		"dynn_Meckema"
		"dynn_Mernseta"
		"dynn_Niding"
		"dynn_Oenema"
		"dynn_Omken"
		"dynn_Ommeloop"
		"dynn_Onsta"
		"dynn_Papinge"
		"dynn_Poder"
		"dynn_Pol"
		"dynn_Popinghe"
		"dynn_Potens"
		"dynn_Reding"
		"dynn_Remian"
		"dynn_Rengers"
		"dynn_Rickwardsma"
		"dynn_Ripperda"
		"dynn_Rodmersma"
		"dynn_Schulte"
		"dynn_Serda"
		"dynn_Sickinge"
		"dynn_Spegel"
		"dynn_Stickel"
		"dynn_Suanckerus"
		"dynn_Swaneke"
		"dynn_Tayingha"
		"dynn_Tiddinga"
		"dynn_Tjarksena"
		"dynn_Ukena"
		"dynn_Umbelap"
		"dynn_Wiemken"
		{ "dynnp_de" "dynn_Aggere" }
		{ "dynnp_de" "dynn_Helpman" }
		{ "dynnp_de" "dynn_Mepsche" }
		{ "dynnp_van" "dynn_Hallum" }
	}
	
	male_names = {
		Arnolt Bart Bazwick Bert Bim Bimble Bomble Bort Borzo Brin Carlin Chomsky Cohlien Cohm Dibbles Dimble Elbin Elgin Famble 
		Fecker Fizwick Gelbin Gibbles Golkin Gorwick Gram Hackett Hewitt Ienkin Jamms Jarnolt Jert Jomble Lando Londo Melbin Mondo 
		Morbin Mordibam Mordibham Nob Obrain Obran Oibran Orbin Orwick Pondo Porda Pordolt Pud Rando Rondo Roy Royan Royeld Royfan 
		Royy Sando Schleemo Sicco Tando Togman Togstrom Torbin Torwick Toshley
	}
	female_names = {
		Alda Amya Anya Ara Ata Bindle Bittsy Brie Cinda Darra Delda Deldi Dothi Edna Elna Ema Emy Erna Ginny Ienka Jubie Kilda Lacie 
		Lacy Maci Mara Mina Mindi Nilla Orra Peanut Penny Petha Pippy Poppet Poppi Serna Sina Tara Tawnie Thimble Tilda Tildy Tilly Tona Topsy Turvy 
		Porda Arna Bardy Berna Bora Brina Carly Winny Royal
	}

	dynasty_of_location_prefix = "dynnp_of"	

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 30
	mat_grf_name_chance = 10
	father_name_chance = 5
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 20
	mat_grm_name_chance = 40
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}
