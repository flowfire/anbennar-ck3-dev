﻿# Accepted categories are: governments, cultures, culture_groups, faiths, religions
# A Flavorization will apply if ALL specified categories have ANY of their entries
# represented in the character/title
# So for example:
# governments = { feudal_government }
# culture_groups = { mongolic_group central_germanic_group }
# religions = { christianity_religion }
# cultures = { norse }
# faiths = { catholicism }
# For this to apply a character MUST BE feudal, AND must also be EITHER altaic OR germanic
# priority is used to determine priority, if they are the same then the LAST will be selected,
# and honorifics will be prefered over landed titles. Default priority is 1
# If a flavorization does not have a tier it can apply to all tiers of characters, too many non-tiered
# flavorizations is bad for performance though, YOU HAVE BEEN WARNED!
# 'top_liege = no' means that its the holder of the title causing the honorific that is tested for
# the various categories, so for a prince it is the king that is tested, for a queen mother it is her son
# if this is not specified ( or 'top_liege = yes' the default ) then it is the top liege of that character
# that is tested, this means that most title flavor comes from the top liege of the title not the holder

#sultan = { #Example
#	gender = male
#	special = holder
#	tier = kingdom
	#	governments = { feudal_government clan_government }
#	religions = { islam_religion }
#}

#############################################

######## TITLE-BASED ########


#Dukes Crossguard of Acromton, Kings if independent
duke_feudal_male_d_acromton = { # Duke-Crossguard
	type = character
	gender = male
	special = holder
	tier = duchy
	priority = 48	#FYI the usual is 47 for alternates
	governments = { feudal_government }
	titles = { d_acromton }
	top_liege = no
}

duke_feudal_female_d_acromton = { 
	type = character
	gender = female
	special = holder
	tier = duchy
	priority = 48
	governments = { feudal_government }
	titles = { d_acromton }
	top_liege = no
}

duke_feudal_male_d_acromton = { # Duke-Crossguard
	type = character
	gender = male
	special = holder
	tier = duchy
	priority = 48	#FYI the usual is 47 for alternates
	governments = { feudal_government }
	titles = { d_acromton }
	top_liege = no
}

duke_feudal_female_d_acromton = { # Why are there two Duke-Crossguard ones?
	type = character
	gender = female
	special = holder
	tier = duchy
	priority = 48
	governments = { feudal_government }
	titles = { d_acromton }
	top_liege = no
}


#High King of Lencenor, like Lorenan
emperor_feudal_male_e_lencenor = { # High King
	type = character
	gender = male
	special = holder
	priority = 48	#FYI the usual is 47 for alternates
	governments = { tribal_government clan_government feudal_government }
	titles = { e_lencenor }
	only_independent = yes
	top_liege = no
}

emperor_feudal_female_e_lencenor = { # High Queen
	type = character
	gender = female
	special = holder
	priority = 48
	governments = { tribal_government clan_government feudal_government }
	titles = { e_lencenor }
	only_independent = yes
	top_liege = no
}

#Moor King of Westmoors
king_feudal_male_k_westmoors_moorman = { # Moor King
	type = character
	gender = male
	special = holder
	priority = 48	#FYI the usual is 47 for alternates
	cultures = { moorman }
	governments = { tribal_government clan_government feudal_government }
	titles = { k_westmoors }
	only_independent = yes
	top_liege = no
}

king_feudal_female_k_westmoors_moorman = { # Moor Queen
	type = character
	gender = female
	special = holder
	priority = 48
	cultures = { moorman }
	governments = { tribal_government clan_government feudal_government }
	titles = { k_westmoors }
	only_independent = yes
	top_liege = no
}

#King of Iochand
king_feudal_male_k_iochand_gnomish_group = { # Iochand is royal title, this is to override Hierarchs and such
	type = character
	gender = male
	special = holder
	priority = 48
	heritages = { heritage_gnomish }
	governments = { tribal_government clan_government feudal_government }
	titles = { k_iochand }
	only_independent = yes
	top_liege = no
}

king_feudal_female_k_iochand_gnomish_group = { # Iochand is royal title, this is to override Hierarchs and such
	type = character
	gender = female
	special = holder
	priority = 48
	heritages = { heritage_gnomish }
	governments = { tribal_government clan_government feudal_government }
	titles = { k_iochand }
	only_independent = yes
	top_liege = no
}

kingdom_feudal_k_iochand = {
	type = title
	tier = kingdom
	priority = 48
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_gnomish }
	titles = { k_iochand }
	only_independent = yes
	top_liege = no
}

######## CULTURAL ###########


#Moon Elf - Feudal here
duke_feudal_male_moon_elvish = {
	type = character
	gender = male
	special = holder
	tier = duchy
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no	#BTW this is to make them Princes even when they are still vassals. If this is not here then they default to Dukes
}
duke_feudal_female_moon_elvish = {
	type = character
	gender = female
	special = holder
	tier = duchy
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}
duchy_feudal_moon_elvish = {
	type = title
	tier = duchy
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}

king_feudal_male_moon_elvish = {
	type = character
	gender = male
	special = holder
	tier = kingdom
	priority = 47
	governments = { feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}
king_feudal_female_moon_elvish = {
	type = character
	gender = female
	special = holder
	tier = kingdom
	priority = 47
	governments = { feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}
kingdom_feudal_moon_elvish = {
	type = title
	tier = kingdom
	priority = 47
	governments = { feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}
emperor_feudal_male_moon_elvish = {	#High Prince
	type = character
	gender = male
	special = holder
	tier = empire
	priority = 102
	governments = { feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}
emperor_feudal_female_moon_elvish = {
	type = character
	gender = female
	special = holder
	tier = empire
	priority = 102
	governments = { feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}
empire_feudal_moon_elvish = {
	type = title
	tier = empire
	priority = 102
	governments = { feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}

#Moon Elf - Republic 
duke_republic_male_moon_elvish = {
	type = character
	gender = male
	special = holder
	tier = duchy
	priority = 28
	governments = { republic_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no	#BTW this is to make them Princes even when they are still vassals. If this is not here then they default to Dukes
}
duke_republic_female_moon_elvish = {
	type = character
	gender = female
	special = holder
	tier = duchy
	priority = 28
	governments = { republic_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}
duchy_republic_moon_elvish = {
	type = title
	tier = duchy
	priority = 28
	governments = { republic_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}

king_republic_male_moon_elvish = {
	type = character
	gender = male
	special = holder
	tier = kingdom
	priority = 47
	governments = { republic_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}
king_republic_female_moon_elvish = {
	type = character
	gender = female
	special = holder
	tier = kingdom
	priority = 47
	governments = { republic_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}
kingdom_republic_moon_elvish = {
	type = title
	tier = kingdom
	priority = 47
	governments = { republic_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}
emperor_republic_male_moon_elvish = {	#High Prince
	type = character
	gender = male
	special = holder
	tier = empire
	priority = 102
	governments = { republic_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}
emperor_republic_female_moon_elvish = {
	type = character
	gender = female
	special = holder
	tier = empire
	priority = 102
	governments = { republic_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}
empire_republic_moon_elvish = {
	type = title
	tier = empire
	priority = 102
	governments = { republic_government }
	heritages = { heritage_elven }
	name_list = { name_list_moon_elven }
	top_liege = no
}

#Wexonard - Feudal : MUST BE ABOVE GENERAL ALENIC
count_feudal_male_wexonard = {	#Lord
	type = character
	gender = male
	special = holder
	tier = county
	priority = 28
	governments = { feudal_government }
	name_list = { name_list_wexonard }
}
count_feudal_female_wexonard = {
	type = character
	gender = female
	special = holder
	tier = county
	priority = 28
	governments = { feudal_government }
	name_list = { name_list_wexonard }
}
county_feudal_wexonard = {
	type = title
	tier = county
	priority = 28
	governments = { feudal_government }
	name_list = { name_list_wexonard }
}
duke_feudal_male_wexonard = {	#Great Lord
	type = character
	gender = male
	special = holder
	tier = duchy
	priority = 28
	governments = { feudal_government }
	name_list = { name_list_wexonard }
}
duke_feudal_female_wexonard = {
	type = character
	gender = female
	special = holder
	tier = duchy
	priority = 28
	governments = { feudal_government }
	name_list = { name_list_wexonard }
}
duchy_feudal_wexonard = {
	type = title
	tier = duchy
	priority = 28
	governments = { feudal_government }
	name_list = { name_list_wexonard }
}

#Alenic - Feudal - Wexonard beats this as they stay as Counts and Dukes btw
count_feudal_male_alenic_group = {	#Lord
	type = character
	gender = male
	special = holder
	tier = county
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_alenic }
}
count_feudal_female_alenic_group = {
	type = character
	gender = female
	special = holder
	tier = county
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_alenic }
}
county_feudal_alenic_group = {
	type = title
	tier = county
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_alenic }
}
duke_feudal_male_alenic_group = {	#Great Lord
	type = character
	gender = male
	special = holder
	tier = duchy
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_alenic }
}
duke_feudal_female_alenic_group = {
	type = character
	gender = female
	special = holder
	tier = duchy
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_alenic }
}
duchy_feudal_alenic_group = {
	type = title
	tier = duchy
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_alenic }
}


#Escanni
count_feudal_male_escanni_group = {	#Lord
	type = character
	gender = male
	special = holder
	tier = county
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_escanni }
}
count_feudal_female_escanni_group = {
	type = character
	gender = female
	special = holder
	tier = county
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_escanni }
}
county_feudal_escanni_group = {
	type = title
	tier = county
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_escanni }
}
duke_feudal_male_escanni_group = {	#HighLord
	type = character
	gender = male
	special = holder
	tier = duchy
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_escanni }
}
duke_feudal_female_escanni_group = {
	type = character
	gender = female
	special = holder
	tier = duchy
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_escanni }
}
duchy_feudal_escanni_group = {
	type = title
	tier = duchy
	priority = 28
	governments = { feudal_government }
	heritages = { heritage_escanni }
}


#Gnomish - Republic (Make em Hierarchies)
king_republic_male_gnomish_group = {
	type = character
	gender = male
	special = holder
	tier = kingdom
	priority = 47
	governments = { republic_government }
	heritages = { heritage_gnomish }
}
king_republic_female_gnomish_group = {
	type = character
	gender = female
	special = holder
	tier = kingdom
	priority = 47
	governments = { republic_government }
	heritages = { heritage_gnomish }
}
kingdom_republic_gnomish_group = {
	type = title
	tier = kingdom
	priority = 28
	governments = { republic_government }
	heritages = { heritage_gnomish }
}
emperor_republic_male_gnomish_group = {
	type = character
	gender = male
	special = holder
	tier = empire
	priority = 47
	governments = { republic_government }
	heritages = { heritage_gnomish }
}
emperor_republic_female_gnomish_group = {
	type = character
	gender = female
	special = holder
	tier = empire
	priority = 47
	governments = { republic_government }
	heritages = { heritage_gnomish }
}
empire_republic_gnomish_group = {
	type = title
	tier = empire
	priority = 28
	governments = { republic_government }
	heritages = { heritage_gnomish }
}

#Gnomish - republic (Make em Hierarchies)
king_republic_male_gnomish_group = {
	type = character
	gender = male
	special = holder
	tier = kingdom
	priority = 47
	governments = { republic_government }
	heritages = { heritage_gnomish }
}
king_republic_female_gnomish_group = {
	type = character
	gender = female
	special = holder
	tier = kingdom
	priority = 47
	governments = { republic_government }
	heritages = { heritage_gnomish }
}
kingdom_republic_gnomish_group = {
	type = title
	tier = kingdom
	priority = 28
	governments = { republic_government }
	heritages = { heritage_gnomish }
}
emperor_republic_male_gnomish_group = {
	type = character
	gender = male
	special = holder
	tier = empire
	priority = 47
	governments = { republic_government }
	heritages = { heritage_gnomish }
}
emperor_republic_female_gnomish_group = {
	type = character
	gender = female
	special = holder
	tier = empire
	priority = 47
	governments = { republic_government }
	heritages = { heritage_gnomish }
}
empire_republic_gnomish_group = {
	type = title
	tier = empire
	priority = 28
	governments = { republic_government }
	heritages = { heritage_gnomish }
}

# Bulwari and Sun Elf Cultural Titles

baron_republic_male_bulwari_group = {
	type = character
	special = holder
	gender = male
	tier = barony
	priority = 8
	governments = { republic_government }
	heritages = { heritage_bulwari }
	top_liege = no
}

baron_republic_female_bulwari_group = {
	type = character
	special = holder
	gender = female
	tier = barony
	priority = 8
	governments = { republic_government }
	heritages = { heritage_bulwari }
	top_liege = no
}

baron_male_bulwari_group = {
	type = character
	special = holder
	gender = male
	tier = barony
	priority = 8
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_bulwari }
	top_liege = no
}

baron_female_bulwari_group = {
	type = character
	special = holder
	gender = female
	tier = barony
	priority = 8
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_bulwari }
	top_liege = no
}

count_male_bulwari_group = {
	type = character
	special = holder
	gender = male
	tier = county
	priority = 28
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_bulwari }
	top_liege = no
}

count_female_bulwari_group = {
	type = character
	special = holder
	gender = female
	tier = county
	priority = 28
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_bulwari }
	top_liege = no
}

duke_male_bulwari_group = {
	type = character
	special = holder
	gender = male
	tier = duchy
	priority = 28
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_bulwari }
	top_liege = no
}

duke_female_bulwari_group = {
	type = character
	special = holder
	gender = female
	tier = duchy
	priority = 28
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_bulwari }
	top_liege = no
}

baron_republic_male_sun_elvish = {
	type = character
	special = holder
	gender = male
	tier = barony
	priority = 8
	governments = { republic_government }
	heritages = { heritage_elven }
	name_list = { name_list_sun_elven }
	top_liege = no
}

baron_republic_female_sun_elvish = {
	type = character
	special = holder
	gender = female
	tier = barony
	priority = 8
	governments = { republic_government }
	heritages = { heritage_elven }
	name_list = { name_list_sun_elven }
	top_liege = no
}

baron_male_sun_elvish = {
	type = character
	special = holder
	gender = male
	tier = barony
	priority = 8
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_sun_elven }
	top_liege = no
}

baron_female_sun_elvish = {
	type = character
	special = holder
	gender = female
	tier = barony
	priority = 8
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_sun_elven }
	top_liege = no
}

count_male_sun_elvish = {
	type = character
	special = holder
	gender = male
	tier = county
	priority = 28
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_sun_elven }
	top_liege = no
}

count_female_sun_elvish = {
	type = character
	special = holder
	gender = female
	tier = county
	priority = 28
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_sun_elven }
	top_liege = no
}

duke_male_sun_elvish = {
	type = character
	special = holder
	gender = male
	tier = duchy
	priority = 28
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_sun_elven }
	top_liege = no
}

duke_female_sun_elvish = {
	type = character
	special = holder
	gender = female
	tier = duchy
	priority = 28
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_sun_elven }
	top_liege = no
}

king_vassal_phoenix_empire = {
	type = character
	special = holder
	gender = male
	tier = kingdom
	priority = 48
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_sun_elven }
	titles = { e_bulwar }
}

queen_vassal_phoenix_empire = {
	type = character
	special = holder
	gender = female
	tier = kingdom
	priority = 48
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_sun_elven }
	titles = { e_bulwar }
}

emperor_feudal_male_sun_elvish = {
	type = character
	special = holder
	gender = male
	tier = empire
	priority = 102
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_sun_elven }
	titles = { e_bulwar }
}

emperor_feudal_female_sun_elvish = {
	type = character
	special = holder
	gender = female
	tier = empire
	priority = 102
	governments = { tribal_government clan_government feudal_government }
	heritages = { heritage_elven }
	name_list = { name_list_sun_elven }
	titles = { e_bulwar }
}

#This stuff needs to be changed into the correct format as per above -Jay
master_skald = {
	type = character
	special = holder
	priority = 102
	governments = { theocracy_government }
	titles = { d_skaldskola }
}

thane = {
	type = character
	special = holder
	priority = 102
	governments = { tribal_government feudal_government }
	tier = county
	heritages = { heritage_gerudian }
}

jarl = {
	type = character
	special = holder
	priority = 102
	governments = { tribal_government feudal_government }
	tier = duchy
	heritages = { heritage_gerudian }
}

konungr = {
	type = character
	special = holder
	priority = 102
	governments = { tribal_government feudal_government }
	tier = kingdom
	heritages = { heritage_gerudian }
	gender = male
}

oztrkonungr = {
	type = charactercons
	special = holder
	priority = 102
	governments = { tribal_government feudal_government }
	tier = empire
	heritages = { heritage_gerudian }
	gender = male
}

drottning = {
	type = character
	special = holder
	priority = 102
	governments = { tribal_government feudal_government }
	tier = kingdom
	heritages = { heritage_gerudian }
	gender = female
}

oztrdrottning = {
	type = character
	special = holder
	priority = 102
	governments = { tribal_government feudal_government }
	tier = empire
	heritages = { heritage_gerudian }
	gender = female
}

