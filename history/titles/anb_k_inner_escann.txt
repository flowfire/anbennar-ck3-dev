k_cast = {
	1000.1.1 = { change_development_level = 8 }
	1020.10.1 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 89 #Wynstan sil Cast
	}
}

k_anor = {
	1000.1.1 = { change_development_level = 8 }
	1020.10.1 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 90 #Lain sil Anor
	}
}

d_nath = {
	1000.1.1 = { change_development_level = 12 }
}

d_sapphirecrown = {
	1000.1.1 = { change_development_level = 10 }
}

d_bradsecker = {
	1000.1.1 = { change_development_level = 10 }
}

d_steelhyl = {
	1000.1.1 = { change_development_level = 9 }
	1020.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_cast"
		holder = 530 #Caylen of Vanbury-Steelhyl, given to him to revitialize Escanni economy + pay dividends to Free Realms
	}
}

d_sapphirecrown = {
	1021.10.31 = {		#Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 90 #Anor King
	}
}

c_aldenmore = {
	1021.10.31 = {		#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_anor"
		holder = 90	#for now
	}
}

c_north_castonath = {
	1000.1.1 = { change_development_level = 22 }
	1020.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_cast"
		holder = 92	#Chlothar of Castonath
	}
}

c_south_castonath = {
	1000.1.1 = { change_development_level = 25 }
	1020.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_anor"
		holder = 90 #Lain sil Anor
	}
}

c_lower_castonath = {
	1000.1.1 = { change_development_level = 20 }
	1020.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_anor"
		holder = 90 #Lain sil Anor
	}
}

#While Cast has the loyalty of the patricians in north castanor, Anor actually has the rest
d_castonath = {
	1020.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_anor"
		holder = 92 #Chlothar of Castonath
		government = republic_government
	}
}

c_ar_urion = {
	1000.1.1 = { change_development_level = 10 }
	1020.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_anor"
		holder = 574 #Lucan Silurion, one of the Silurion quintuplets
	}
}

c_southgate = {
	1000.1.1 = { change_development_level = 10 }
	1020.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_anor"
		holder = 574 #Lucan Silurion, one of the Silurion quintuplets
	}
}

d_southgate = {
	1020.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_anor"
		holder = 574 #Lucan Silurion, one of the Silurion quintuplets
	}
}

d_foarhal = {
	1000.1.1 = { change_development_level = 11 }
	1020.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_anor"
		holder = 514 #Iacob the Betrayer
	}
}

c_foarhal = {
	1000.1.1 = { change_development_level = 12 }
}

c_bradnath = {
	1000.1.1 = { change_development_level = 12 }
	1020.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_anor"
		holder = 514 #Iacob the Betrayer
	}
}

c_mintirley = {
	1000.1.1 = { change_development_level = 8 }
	1020.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_anor"
		holder = 514 #Iacob the Betrayer
	}
}

c_marronath = {
	1000.1.1 = { change_development_level = 10 }
	1020.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = "k_anor"
		holder = 514 #Iacob the Betrayer
	}
}

c_the_manorfields = {
	1000.1.1 = { change_development_level = 11 }
}

d_westgate = {
	1000.1.1 = { change_development_level = 10 }
}

c_westgate = {
	1000.1.1 = { change_development_level = 11 }
}

c_ardent_keep = {
	1000.1.1 = { change_development_level = 9 }
}

c_the_north_citadel = {
	1000.1.1 = { change_development_level = 12 }
}

c_khugsroad = {
	1000.1.1 = { change_development_level = 10 }	#cos of dwarf
}